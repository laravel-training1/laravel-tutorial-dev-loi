@extends('Layout.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>CRUD CART</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('cart.create') }}"> Create New Cart</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>User</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($cart as $value)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $value->user }}</td>
                <td>
                    <form action="{{ route('cart.destroy',$value->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('cart.show',$value->id) }}">Add Product</a>

                        <a class="btn btn-primary" href="{{ route('cart.edit',$value->id) }}">Edit</a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $cart->links() !!}

@endsection
