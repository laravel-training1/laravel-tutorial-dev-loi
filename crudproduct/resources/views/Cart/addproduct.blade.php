@extends('Layout.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>User : {{$carts->user}}</h2>
            </div>

        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>Name</th>
            <th>Details</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($carts->product as $value)
            <tr>
                <td>{{ $value->name }}</td>
                <td>{{ $value->detail }}</td>
                <td>
                    <a class="btn btn-danger" href="{{ Url('/')}}/delete/{{$value->id}}/{{$carts->id}}">Delete</a>
                </td>
            </tr>
        @endforeach
    </table>
    <h4>All Product</h4>
    <table class="table table-bordered">
        <tr>
            <th>Name</th>
            <th>Details</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($product as $value)
            <tr>
                <td>{{ $value->name }}</td>
                <td>{{ $value->detail }}</td>
                <td>
                    <a class="btn btn-primary" href="{{ Url('/')}}/saveitem/{{$value->id}}/{{$carts->id}}">Add to Cart</a>
                </td>
            </tr>
        @endforeach
    </table>



@endsection
