<!DOCTYPE html>
<html>
<head>
    <title>Laravel 8 CRUD</title>
{{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >--}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
</head>
<body>

<div class="container">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="navbar-brand mb-0 h1"  href="{{ route('category.index') }}">Category <span class="sr-only"></span></a>
                </li>
                <li class="nav-item active">
                    <a class="navbar-brand mb-0 h1"  href="{{ route('products.index') }}">Product <span class="sr-only"></span></a>
                </li>
                <li class="nav-item active">
                    <a class="navbar-brand mb-0 h1"  href="{{ route('cart.index') }}">Cart <span class="sr-only"></span></a>
                </li>
            </ul>
        </div>
    </nav>
    @yield('content')
</div>

</body>
</html>
