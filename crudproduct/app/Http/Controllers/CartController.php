<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use App\Models\Product_Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        $cart = Cart::latest()->paginate(5);

        return view('cart.index',compact('cart'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('cart.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'user' => 'required',
        ]);

        Cart::create($request->all());

        return redirect()->route('cart.index')
            ->with('success','Cart created successfully.');
    }

    public function show(Cart $cart)
    {
        $carts = $cart::with('product' )->find($cart->id);
        $product = Product::all();
        return view('cart.addproduct',compact('carts','product'));
    }


    public function edit(Cart $cart)
    {
        return view('cart.edit',compact('cart'));
    }


    public function update(Request $request, Cart $cart)
    {
        $request->validate([
            'user' => 'required',
        ]);

        $cart->update($request->all());

        return redirect()->route('cart.index')
            ->with('success','Cart updated successfully');
    }


    public function destroy(Cart $cart)
    {
        $cart->delete();
        return redirect()->route('cart.index')
            ->with('success','Cart deleted successfully');
    }
    public function saveitem($productId,$cartId){
       if(Product_Cart::where('cartId',$cartId)->where('productId',$productId)->count() == 0){
           Product_Cart::create([
               'cartId'=>$cartId,
               'productId'=>$productId,
           ]);
       };
        return redirect()->back();
    }
    public function delitem($productId,$cartId){
        Product_Cart::where('cartId',$cartId)->where('productId',$productId)->delete();
        return redirect()->back();
    }
}
