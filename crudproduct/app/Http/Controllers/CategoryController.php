<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {
        $category = Category::latest()->paginate(5);

        return view('category.index',compact('category'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);

        Category::create($request->all());

        return redirect()->route('category.index')
            ->with('success','Category created successfully.');
    }

    public function show(Category $category)
    {
        return view('category.show',compact('category'));
    }


    public function edit(Category $category)
    {
        return view('category.edit',compact('category'));
    }


    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);

        $category->update($request->all());

        return redirect()->route('category.index')
            ->with('success','Category updated successfully');
    }


    public function destroy(Category $category)
    {
        if ($category->product()->count() > 0){
            $success = "Can't delete because there are products !";
        }else{
            $category->delete();
            $success = "Category deleted successfully";
        }
        return redirect()->route('category.index')
            ->with('success',$success);
    }
}
