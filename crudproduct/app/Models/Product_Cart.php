<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product_Cart extends Model
{
    use HasFactory;
    protected $table = "product_cart";
    protected $primaryKey = "id";
    public $timestamps = false;
    protected $fillable = [
        'cartId', 'productId'
    ];

}
