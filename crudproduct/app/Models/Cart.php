<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $table = "cart";
    protected $primaryKey = "id";
    public $timestamps = false;
    protected $fillable = [
        'user'
    ];
    public function product_cart(){
        return $this->hasMany('App\Models\product_cart','CartId','id');
    }
    public function product(){
        return $this->belongsToMany('App\Models\product', 'product_cart', 'CartId', 'productId');
    }
}
