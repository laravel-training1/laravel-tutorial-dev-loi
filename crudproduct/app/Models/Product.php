<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = "products";
    protected $primaryKey = "id";
    public $timestamps = false;
    protected $fillable = [
        'name', 'detail','categoryId'
    ];

    public function category(){
        return $this->belongsTo('App\Models\Category','categoryId','id');
    }
    public function product_cart(){
        return $this->hasMany('App\Models\product_cart','productId','id');
    }
    public function cart(){
        return $this->belongsToMany('App\Models\cart', 'product_cart', 'productId', 'CartId');
    }

}
